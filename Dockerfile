#FROM alpine:latest
FROM node:16-alpine

MAINTAINER Leo Cheron <leo@cheron.works>

# build dependencies
#RUN apk --no-cache add python build-base

# nodejs
#RUN apk --no-cache add nodejs nodejs-npm

# zip
RUN apk --no-cache add zip curl git python3 make g++

# composer
RUN apk --no-cache --repository=http://dl-4.alpinelinux.org/alpine/edge/testing add \
	composer \
	curl \
    openssl \
    php7 \
    php7-xml \
    php7-xsl \
    php7-pdo \
    php7-pdo_mysql \
    php7-mcrypt \
    php7-curl \
    php7-json \
    php7-fpm \
    php7-phar \
    php7-openssl \
    php7-mysqli \
    php7-ctype \
    php7-opcache \
    php7-mbstring \
    php7-session \
    php7-pdo_sqlite \
    php7-sqlite3 \
    php7-tokenizer \
    php7-xmlwriter \
    php7-simplexml \
    php7-pcntl

# PHP Code Sniffer
RUN curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar && \
    cp ./phpcs.phar /usr/local/bin/phpcs && \
    chmod +x /usr/local/bin/phpcs

RUN /usr/local/bin/phpcs --config-set show_progress 1 && \
    /usr/local/bin/phpcs --config-set colors 1 && \
    /usr/local/bin/phpcs --config-set report_width 140 && \
    /usr/local/bin/phpcs --config-set encoding utf-8

# gulp
RUN npm i -g gulp

RUN mkdir -p /data
VOLUME /data
WORKDIR /data

CMD ["sh"]
